# -*- coding: utf-8 -*-
import http.client
import urllib
import csv
import argparse
import sys
import json
from abc import abstractmethod

#####################################################################
# Response Data
#####################################################################

class Data:
    def __init__(self, name, age, count):
        self.name = name
        self.age = age
        self.count = count
        
#####################################################################
# Output Classes
#####################################################################

class OutputDataSet:
    @abstractmethod
    def write(self, data):
        """ """

class FileOutputTxt(OutputDataSet):
    def __init__(self, file_path, delimiter = ';'):
        self.file = open(file_path, 'w', errors = 'ignore', encoding = 'utf-8')
        self.delimiter = delimiter
        self.file.write(f'name{self.delimiter}age{self.delimiter}count\n')

    def write(self, data):
        self.file.write(f'{data.name}{self.delimiter}{data.age}{self.delimiter}{data.count}\n')

    def __del__(self):
        self.file.close()

class FileOutputCsv(OutputDataSet):
    def __init__(self, file_path, delimiter = ';'):
        self.file = open(file_path, 'w', encoding = 'utf-8')
        self.file_writer = csv.writer(self.file, delimiter = delimiter, lineterminator = '\r')
        self.file_writer.writerow(['name', 'age', 'count'])

    def write(self, data):
        self.file_writer.writerow([data.name, str(data.age), str(data.count)])

    def __del__(self):
        self.file.close()
        

#####################################################################
# Main
#####################################################################

if __name__ == "__main__":

    # set console arguments
    args_parser = argparse.ArgumentParser(description='Extract list of names with unique attributes and save to file')
    args_parser.add_argument('-input_file', type = str, default = '', help = 'Path input file')
    args_parser.add_argument('-output_format', type = str, default = '', help = 'Path output file format: txt or csv')
    args_parser.add_argument('-output_file', type = str, default = '', help = 'Path output file without extension')
    args = args_parser.parse_args()

    if ((not args.input_file) or (not args.output_format) or (not args.output_file)
        or (args.output_format != 'txt' and args.output_format != 'csv')
        ):
        print('Arguments not contain input or output data keys.')
        exit(0)

    set_of_names = set()

    try:
        with open(args.input_file, 'r', errors = 'ignore', encoding = 'utf-8') as input_file:

            # create output data
            if args.output_format == 'txt':
                output_data = FileOutputTxt(args.output_file + '.txt')
            else:
                output_data = FileOutputCsv(args.output_file + '.csv')

            i = 0
            try:
                connection = http.client.HTTPSConnection('api.agify.io')
                connection.connect()
            except:
                print('Connnection failed')
                exit(0)

            for line in input_file.readlines():
                print(f'\rGet info for name {i + 1}', end = '', flush = True)
                i = i + 1

                if line.strip() in set_of_names:
                    continue

                set_of_names.add(line.strip())
                req = '/?name=' + urllib.parse.quote(line.strip()) # convert to percent-encoding

                try:
                    connection.request('GET', req)

                    response = connection.getresponse()
                    body = response.read()
                    response_text = body.decode('utf-8')

                    if response_text == '{"error":"Request limit reached"}':
                        print('\n{"error":"Request limit reached"}')
                        break
                
                    if response_text == '''{"error":"Missing 'name' parameter"}''':
                        continue            
                except:
                    print('\nFailed request')
                    break

                result = json.loads(response_text)
                data = Data(result['name'], result['age'], result['count'])

                if data.age == None:
                    continue

                output_data.write(data)
    except:
        e = sys.exc_info()
        print(e)
    