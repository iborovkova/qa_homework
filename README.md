# README #

### Program description: ###
The program receives three parameters as input: the path to the file with names, the format of the output file and the path to the output file. An API request is made for each name, and the results are written to the specified output file. The output file format can be csv or txt. All three parameters must be set for the program to work.

### Examples of starting the program: ###
* python homework.py -output_format=txt -input_file='Names.txt' -output_file=output_file
* python homework.py -output_format=csv -input_file='Names.txt' -output_file=output_file

### Reference: ###
* python homework.py -h
